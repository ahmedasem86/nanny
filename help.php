<!DOCTYPE html>
<html lang="en">
<?php include 'head.php';?>


<body>

	<nav class="navbar navbar-expand-lg top_nav">
			<div class="container">
				<a class="navbar-brand" href="#"><img src="images/logo.jpeg" alt="Logo: Purple True Mark" class="logo"/></a>
				 
				<div class="top_nav_left"> <?php include 'top_nav_left.php';?> </div>
       			<div class="top_nav_right"> <?php include 'top_nav_right.php';?> </div>
			</div>
		</nav>
	<!---about Section Start-->
	<section class="article" id="help" style="min-height:661px;">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="heading-col">
						<h1>Welcome Poppins..♥</h1>
						<h2>Our services</h2>
						<div class="border">We offer a range of services for nannies in terms of:</div>
						<div class="gap-30"></div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-4 col-sm-6">
					<div class="media">
						<div class="media-body">
							<h4 class="media-heading">Reminder day salary.</h4>
							<p>Reminder day salary for our maids and users.</p>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="media">
						<div class="media-body">
							<h4 class="media-heading">Distribution of household tasks</h4>
							<p>Protect you from forgetting maid tasks</p>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="media">
					
						<div class="media-body">
							<h4 class="media-heading">Motivation </h4>
							<p>Motivating the performance of tasks in terms of incentive rewards</p>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="media">
					  	<div class="media-body">
					    	<h4 class="media-heading">Annual bonuses</h4>
							<p>Calculate annual bonuses for maids depending on their target and work</p>
					  	</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">				
					<div class="media">
						<div class="media-left media-middle">
						  <p><i class="fa fa-medkit"></i></p>
						</div>
						<div class="media-body">
							<h4 class="media-heading">Calendar</h4>
							<p>Calendar for daily dated and monthly dated tasks</p>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">				
					<div class="media">
						<div class="media-left media-middle">
						  <p><i class="fa fa-medkit"></i></p>
						</div>
						<div class="media-body">
							<h4 class="media-heading">Calendar</h4>
							<p>Calendar for daily dated and monthly dated tasks</p>
						</div>
					</div>
				</div>	
			</div>
		</div>
	</section>

	<footer id="footer" class="footer"> <?php include 'footer.php';?> </footer>
	
</body>

</html>
