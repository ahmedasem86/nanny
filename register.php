<!DOCTYPE html>
<html lang="en">
<?php include 'head.php';?>


<body>

	<nav class="navbar navbar-expand-lg top_nav">
			<div class="container">
				<a class="navbar-brand" href="#"><img src="images/logo.jpeg" alt="Logo: Purple True Mark" class="logo"/></a>
				 
				<div class="top_nav_left"> <?php include 'top_nav_left.php';?> </div>
       			<div class="top_nav_right"> <?php include 'top_nav_right.php';?> </div>
			</div>
		</nav>
	
        <section id ="register" class="article">
		<?php if(!empty($GLOBALS['errors'])): ?>
			<div class="alert alert-danger text-center" role="alert">
				<?php foreach($GLOBALS['errors'] as $error): ?>
					<div class="colmd-6"><?= $error ?></div> <br>
				<?php endforeach;?>
			</div>
			<?php endif;?>
			<h1> Register Now ...</h1>

			
			<form action="" method="post" style="padding-left:520px; min-height:670px;" enctype="multipart/form-data">
			<table>
			<tr>	<th>User name: </th>	<td><input  class="form-control" type="text" name="username" size ="30" maxlength="50" required></td>	</tr>
			<tr>	<th>Full name: </th>	<td><input  class="form-control" type="text" name="fullname" size ="30" maxlength="50"required></td>	</tr>
			<tr>	<th>Email: </th>	<td><input  class="form-control" type="email" name="email" size ="30" maxlength="100"required></td>	</tr>
			<tr>	<th>Phone: </th>	<td><input  class="form-control" type="text" name="phone" size ="30" maxlength="30"required></td>	</tr>
			<tr>	<th>Address: </th>	<td><input  class="form-control" type="text" name="address" size ="30" maxlength="100" required></td>	</tr>
			<tr>	<th>Password: </th>	<td><input   class="form-control" type="password" name="password" size ="30" maxlength="30" required></td>	</tr>
			<tr>	<th>Confirm Password: </th>	<td><input  class="form-control" type="password" name="password_confirmation" size ="30" maxlength="30" required></td>	</tr>
			<input type="hidden" name="is_parent" value="1">
			<tr> <td colspan="2"><input type="submit"  class="form-control" name="register" value ="Register"></td> </tr>
			</table>
			
		</form>

        </section>

	<footer id="footer" class="footer"> <?php include 'footer.php';?> </footer>
	
</body>

</html>
