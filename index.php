<!DOCTYPE html>
<html lang="en">
<?php include 'head.php';?>
<body>

	<nav class="navbar navbar-expand-lg top_nav">
			<div class="container">
				<a class="navbar-brand" href="#"><img src="images/logo.jpeg" alt="Logo: Purple True Mark" class="logo"/></a>
				 
				<div class="top_nav_left"> <?php include 'top_nav_left.php';?> </div>
       			<div class="top_nav_right"> <?php include 'top_nav_right.php';?> </div>
			</div>
		</nav>
		<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="images/news1.jpeg"  style="max-height:400px;" class="d-block w-100" alt="...">
    </div>
    <div class="carousel-item">
      <img src="images/news2.jpeg" style="max-height:400px;" class="d-block w-100" alt="...">
    </div>
    <div class="carousel-item">
      <img src="images/news3.jpeg" style="max-height:400px;" class="d-block w-100" alt="...">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

	<!---about Section Start-->
	<section class="article" id="about">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="heading-col">
						<h1>Welcome Poppins..♥</h1>
						<h2>Our services</h2>
						<div class="border">We offer a range of services for nannies in terms of:</div>
						<div class="gap-30"></div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-4 col-sm-6">
					<div class="media">
						<div class="media-body">
							<h4 class="media-heading">Reminder day salary.</h4>
							<p>Reminder day salary for our maids and users.</p>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="media">
						<div class="media-body">
							<h4 class="media-heading">Distribution of household tasks</h4>
							<p>Protect you from forgetting maid tasks</p>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="media">
					
						<div class="media-body">
							<h4 class="media-heading">Motivation </h4>
							<p>Motivating the performance of tasks in terms of incentive rewards</p>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="media">
					  	<div class="media-body">
					    	<h4 class="media-heading">Annual bonuses</h4>
							<p>Calculate annual bonuses for maids depending on their target and work</p>
					  	</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">				
					<div class="media">
						<div class="media-left media-middle">
						  <p><i class="fa fa-medkit"></i></p>
						</div>
						<div class="media-body">
							<h4 class="media-heading">Calendar</h4>
							<p>Calendar for daily dated and monthly dated tasks</p>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">				
					<div class="media">
						<div class="media-left media-middle">
						  <p><i class="fa fa-medkit"></i></p>
						</div>
						<div class="media-body">
							<h4 class="media-heading">Calendar</h4>
							<p>Calendar for daily dated and monthly dated tasks</p>
						</div>
					</div>
				</div>	
			</div>
		</div>
	</section>
	<section class="article" id="news">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="heading-col">
						<h2>LATEST News</h2>
						<div class="border"></div>
						<p class="down">See all Nanies latest news</p>
					</div>
				</div>
			</div>
			<div class="row">

				<?php $all_news = $newsController->getNews(); 
				foreach($all_news as $news):
				?>
				<div class="col-sm-6 col-md-4">
				    <div class="thumbnail">
				      	<img src="<?= $GLOBALS['APP_URL']."/control".$news['image'];?>" class="news_image"alt="">
				      	<div class="caption">
					        <a href="news_details.php?news_id=<?=$news['id']?>"><h4><?= $news['title']?></h4></a>
					        <div class="comment">
					        	<ul>
					        		<li>
					        			<i class="fa fa-calendar" style="margin-right: 5px;"></i ><span><?= $news['date']?></span>
					        		</li>
					        	</ul>
					        </div>
						     <p class="all"><?= $news['paragraph']?></p>
						     <p class="submit"><a href="news_details.php?news_id=<?=$news['id']?>" class="btn read_more">Read More</a></p>
				      	</div>
				    </div>
				</div>
				<?php endforeach; ?>
			</div>
		</div>
	</section>


	<footer id="footer" class="footer"> <?php include 'footer.php';?> </footer>
	
</body>

</html>