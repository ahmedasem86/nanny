<!DOCTYPE html>
<html lang="en">
<?php include 'head.php';

$news = $newsController->getnewsById($_GET['news_id']);
?>

<body>

	<nav class="navbar navbar-expand-lg top_nav">
			<div class="container">
				<a class="navbar-brand" href="#"><img src="images/logo.jpeg" alt="Logo: Purple True Mark" class="logo"/></a>
				 
				<div class="top_nav_left"> <?php include 'top_nav_left.php';?> </div>
       			<div class="top_nav_right"> <?php include 'top_nav_right.php';?> </div>
			</div>
		</nav>
	<!---about Section Start-->
	<section class="article" id="help" style="min-height:661px;">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="heading-col">
						<h1><?= $news['title']?> </h1>
                        <img  style="margin-left:30%;"width="500" height="500" src="<?= $GLOBALS['APP_URL'] . '/control/' . $news['image']?>" alt="">
                        <div class="col-md-12">
                        <b>Authored in: </b> <small><?= $news['date']?></small>
                        </div>
						<div class="border"><?=  $news['paragraph'];?></div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<footer id="footer" class="footer"> <?php include 'footer.php';?> </footer>
	
</body>

</html>
