<head>
    <?php 
    $GLOBALS['errors'] = [];


    session_start();
    if(isset($_GET['logout'])){
        unset($_SESSION['id']);
        header('location: login.php');
    }
        include('control/AboutControl.php'); 
        include('control/AuthControl.php'); 
        include('control/PaymentControl.php'); 
        include('control/NewsControl.php'); 
        include('control/TaskControl.php'); 

    ?>
    <title> Nanny Poppins </title>
        <link type="text/css" rel="stylesheet" href="style.css">
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" integrity="undefined" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>    
</head>
