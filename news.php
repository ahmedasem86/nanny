<!DOCTYPE html>
<html lang="en">
<?php include 'head.php';?>
<body>

	<nav class="navbar navbar-expand-lg top_nav">
			<div class="container">
				<a class="navbar-brand" href="#"><img src="images/logo.jpeg" alt="Logo: Purple True Mark" class="logo"/></a>
				 
				<div class="top_nav_left"> <?php include 'top_nav_left.php';?> </div>
       			<div class="top_nav_right"> <?php include 'top_nav_right.php';?> </div>
			</div>
		</nav>

        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                <img src="images/news4.jpeg" class="d-block w-100" style="max-height:400px;"alt="...">
                </div>
            </div>
         </div>
	<section class="article" id="news">
		<div class="container">
			<div class="row">
                <h1><i class="far fa-newspaper"></i>  Our News </h1>
				<?php $all_news = $newsController->getNews(); 
				foreach($all_news as $news):
				?>
				<div class="col-sm-6 col-md-4">
				    <div class="thumbnail">
				      	<img src="<?= $GLOBALS['APP_URL']."/control".$news['image'];?>" class="news_image"alt="">
				      	<div class="caption">
					        <a href="news_details.php?news_id=<?=$news['id']?>"><h4><?= $news['title']?></h4></a>
					        <div class="comment">
					        	<ul>
					        		<li>
					        			<i class="fa fa-calendar" style="margin-right: 5px;"></i ><span><?= $news['date']?></span>
					        		</li>
					        	</ul>
					        </div>
						     <p class="all"><?= $news['paragraph']?></p>
						     <p class="submit"><a href="news_details.php?news_id=<?=$news['id']?>" class="btn read_more">Read More</a></p>
				      	</div>
				    </div>
				</div>
				<?php endforeach; ?>
			</div>
		</div>
	</section>


	<footer id="footer" class="footer"> <?php include 'footer.php';?> </footer>
	
</body>

</html>