<!DOCTYPE html>
<html lang="en">
<?php include '../head.php';?>

<body>

	<nav class="navbar navbar-expand-lg top_nav">
			<div class="container">
				<a class="navbar-brand" href="#"><img src="../images/logo.jpeg" alt="Logo: Purple True Mark" class="logo"/></a>
				 
				<div class="top_nav_left"> <?php include '../top_nav_left.php';?> </div>
       			<div class="top_nav_right"> <?php include '../top_nav_right.php';?> </div>
			</div>
		</nav>
	<!---about Section Start-->
	<section class="article" id="help" style="min-height:661px;">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
            <?php include('header.php');
			$paymenttypes = $paymentController->getPaymentType(); 
			?>
				</div>
            </div>
            <div class="container mt-5 ">
                <h4>Add Payment</h4>
                <br>
            	<form action="<?=$GLOBALS['APP_URL']?>/dashboard/view_payments.php" method="post" style="padding-left:420px; min-height:670px;" enctype="multipart/form-data">
			<table>
			<tr>	<th>Payment Purpose: </th>	<td><input  class="form-control" type="text" name="purpose" size ="30" maxlength="50" required></td>	</tr>
			<tr>	<th>Payment Description: </th>	<td><textarea class="form-control" name="description" id="" cols="30" rows="10" required></textarea></td>	</tr>		 
			<tr>	<th>Payment Amount: </th>	<td><input  class="form-control" type="number" name="amount" size ="30" maxlength="50" required></td>	</tr>
			<tr>	<th>Payment Date: </th>	<td><input type="datetime-local" name="paidDate" required></td>	</tr>		
			<tr>	<th>Payment Type: </th>	
				<td>
				<select name="paymenttype" class="form-control">
					<?php foreach($paymenttypes as $payment_type): ?>
						<option value="<?=$payment_type['id']?>"><?= $payment_type['type']?></option>
						<?php endforeach; ?>
					</select>
				</td>
				</tr>		

			<input type="hidden" name="userId" value="<?= $_GET['owner_id']?>">
            <input type="hidden" name="maidId" value="<?= $_GET['maid_id']?>">
            <input type="hidden" name="status" value="0">
            <tr> <td colspan="2"><input type="submit"  class="form-control" name="addPayment" value ="Add Payment"></td> </tr>
			</table>
					</form>
			</div>
	</section>
	<footer id="footer" class="footer"> <?php include '../footer.php';?> </footer>
	
</body>

</html>
