<!DOCTYPE html>
<html lang="en">
<?php include '../head.php';?>

<body>

	<nav class="navbar navbar-expand-lg top_nav">
			<div class="container">
				<a class="navbar-brand" href="#"><img src="../images/logo.jpeg" alt="Logo: Purple True Mark" class="logo"/></a>
				 
				<div class="top_nav_left"> <?php include '../top_nav_left.php';?> </div>
       			<div class="top_nav_right"> <?php include '../top_nav_right.php';?> </div>
			</div>
		</nav>
	<!---about Section Start-->
	<section class="article" id="help" style="min-height:661px;">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
            <?php include('header.php') ?>
				</div>
            </div>
            <?php   $news = $newsController->getnewsById($_GET['id']);?>

            <div class="container mt-5 ">
                <h4>Editing News <span class="color_primary"><?= $news['title'];?></span> by the author<small class="color_primary"> <?= $news['username'];?></small></h4>
                <br>
                <form class="row" action="<?=$GLOBALS['APP_URL']?>/dashboard/view_news.php" method="post" enctype="multipart/form-data">
                <table>
                    <tr>	<th>Title: </th>	<td><input  class="form-control" type="text" value="<?= $news['title'];?>" name="title" size ="30" maxlength="50" required></td>	</tr>
                    <tr>	<th>Paragraph: </th>	<td><textarea  class="form-control" name="paragraph" required ><?= $news['paragraph'];?></textarea></td>	</tr>
                    <tr>	<th>Image: </th>	<td align="center"> 
                        <img  src="<?= "http://".$_SERVER['SERVER_NAME']."/control".$news['image'];?>" alt="" width="90" height="90">  
                        <input  class="form-control" type="file"  name="image" ></td>	</tr>
                    <tr>	<th>Date: </th>	<td><input  class="form-control" type="date" value="<?= $news['date'];?>" name="date" required></td>	</tr>
                    <input type="hidden" name="news_id" value="<?= $news['id'];?>">
                    <tr> <td colspan="2"><input type="submit"  class="form-control" name="update_news" value ="Update"></td> </tr>
                    </table>
                </form>
			</div>
	</section>
	<footer id="footer" class="footer"> <?php include '../footer.php';?> </footer>
	
</body>

</html>
