<!DOCTYPE html>
<html lang="en">
<?php include '../head.php';?>

<body>

	<nav class="navbar navbar-expand-lg top_nav">
			<div class="container">
				<a class="navbar-brand" href="#"><img src="../images/logo.jpeg" alt="Logo: Purple True Mark" class="logo"/></a>
				 
				<div class="top_nav_left"> <?php include '../top_nav_left.php';?> </div>
       			<div class="top_nav_right"> <?php include '../top_nav_right.php';?> </div>
			</div>
		</nav>
	<!---about Section Start-->
	<section class="article" id="help" style="min-height:661px;">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
            <?php include('header.php') ;
            $paymenttypes = $paymentController->getPaymentType(); 
            ?>
				</div>
            </div>
            <?php   $payment = $paymentController->getPaymentById($_GET['id']);?>

            <div class="container mt-5 ">
                <h4>Editing Payment  <span class="color_primary"><?= $payment['purpose'];?></span></h4>
                <br>
                <form class="row" action="<?=$GLOBALS['APP_URL']?>/dashboard/view_payments.php" method="post">
                <table>
			<tr>	<th>Payment Purpose: </th>	<td><input  class="form-control" type="text" name="purpose" value="<?= $payment['purpose'];?>" size ="30" maxlength="50" required></td>	</tr>
			<tr>	<th>Payment Description: </th>	<td><textarea class="form-control" name="description"  id="" cols="30" rows="10" required><?= $payment['description'];?></textarea></td>	</tr>		 
			<tr>	<th>Payment Amount: </th>	<td><input  class="form-control" type="number" name="amount" value="<?= $payment['amount'];?>" size ="30" maxlength="50" required></td>	</tr>
			<tr>	<th>Payment Type: </th>	
				<td>
				<select name="paymenttype" class="form-control">
					<?php foreach($paymenttypes as $payment_type): ?>
						<option value="<?=$payment_type['id']?>" <?= ($payment['paymenttype'] == $payment_type['id'])? 'selected' : ''; ?>><?= $payment_type['type']?></option>
						<?php endforeach; ?>
					</select>
				</td>
				</tr>	
                <input type="hidden" name="payment_id" value="<?= $payment['paymentId']?>">	
            <tr> <td colspan="2"><input type="submit"  class="form-control" name="update_payment" value ="Update Payment"></td> </tr>
			</table>
                </form>
			</div>
	</section>
	<footer id="footer" class="footer"> <?php include '../footer.php';?> </footer>
	
</body>

</html>
