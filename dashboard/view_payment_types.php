<!DOCTYPE html>
<html lang="en">
<?php include '../head.php';?>

<body>

	<nav class="navbar navbar-expand-lg top_nav">
			<div class="container">
				<a class="navbar-brand" href="#"><img src="../images/logo.jpeg" alt="Logo: Purple True Mark" class="logo"/></a>
				 
				<div class="top_nav_left"> <?php include '../top_nav_left.php';?> </div>
       			<div class="top_nav_right"> <?php include '../top_nav_right.php';?> </div>
			</div>
		</nav>
	<!---about Section Start-->
	<section class="article" id="help" style="min-height:661px;">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
            <?php include('header.php') ?>
				</div>
            </div>
            <div class="container mt-5 ">
            <a href="<?=$GLOBALS['APP_URL']?>/dashboard/add_payment_type.php" class=" col-md-3 nav-background add_new_btn">Add Payment Type</a>

              <table class="table table-stripped nav-background text-white mt-20">
                <thead>
                    <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Name</th>
                    <th scope="col" class="text-center">Edit</th>
                    <th scope="col" class="text-center">Delete</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                    $payment_types = $paymentController->getPaymentType();
                    if($payment_types != null):
                        
                    foreach($payment_types as $type):?>
                    <tr>
                    <th scope="row"><?= $type['id'];?></th>
                    <td><?= $type['type'];?></td>
                    <td class="text-center"><a href="edit_payment_type.php?id=<?= $type['id'];?>" class="text-white "><i class="far fa-edit"></i></a></td>
                    <td class="text-center">
                    <form action="<?=$GLOBALS['APP_URL']?>/dashboard/view_payment_types.php" method="post">
                    <input type="hidden" name="type_id" value="<?= $type['id'];?>">
                        <button type="submit" name="delete_payment_type" class="button_trans"><i class="far fa-trash"></i></button>
                        </form>
                    </td>
                    </tr>
                    <?php endforeach; 
                    else:
                    ?>
                    <tr><td colspan="2" class="text-center"> No Payment Types added</td></tr>
                    <?php endif; ?>
                </tbody>
                </table>
			</div>
	</section>
	<footer id="footer" class="footer"> <?php include '../footer.php';?> </footer>
	
</body>

</html>
