<!DOCTYPE html>
<html lang="en">
<?php include '../head.php';?>

<body>
<style>
    .add_new_btn{
    color: white;
    text-decoration: inherit;
    padding: 5px;
    border: 1px solid black;
    box-shadow: 2px 2px #ccc;
}
.nav-background{
    background-color: #8c2620cc;
}
</style>
	<nav class="navbar navbar-expand-lg top_nav">
			<div class="container">
				<a class="navbar-brand" href="#"><img src="../images/logo.jpeg" alt="Logo: Purple True Mark" class="logo"/></a>
				 
				<div class="top_nav_left"> <?php include '../top_nav_left.php';?> </div>
       			<div class="top_nav_right"> <?php include '../top_nav_right.php';?> </div>
			</div>
		</nav>
	<!---about Section Start-->
	<section class="article" id="help" style="min-height:661px;">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
            <?php include('header.php') ?>
				</div>
            </div>
            <div class="container mt-5 ">
                <?php if(isset($_SESSION['role']) && $_SESSION['role'] == 1): ?>
                   <h3>My maids tasks</h3> 
                <?php elseif(isset($_SESSION['role']) && $_SESSION['role'] == 2): ?>
                    <h3> My Work tasks</h3>  
                <?php endif;?>
                <form action="" class="row col-md-12">
				
					<div class="form-group col-md-4">
						<input type="search" placeholder="Search by Task name" name="key_word" id="form1" value="<?= (isset($_GET['key_word']))? $_GET['key_word'] : '';?>" class="form-control " />
					</div>
					<div class="col-md-2">
						<button type="submit" class="btn btn-primary nav-background add_new_btn ">
							Search by Task name <i class="fas fa-search"></i>
						</button>
					</div>
			</form>
              <table class="table table-stripped nav-background text-white mt-20">
                <thead>
                    <tr>
                    <th scope="col">#</th>
                    <th scope="col">Title</th>
                    <th scope="col">Description</th>
                    <th scope="col">Maid</th>
                    <th scope="col">Owner</th>
                    <th scope="col">Status</th>
                    <th scope="col">Task date</th>
                    <?php if(isset($_SESSION['role']) && $_SESSION['role'] == 1): ?>
                     <th  class="text-center" scope="col">Delete</th>
                     <?php elseif(isset($_SESSION['role']) && $_SESSION['role'] == 2): ?>
                      <th class="text-center" scope="col">Complete task</th>
                     <?php endif; ?>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                    $tasks = $taskController->getTasks();
                    foreach($tasks as $task): ?>
                    <tr>
                    <td><?= $task['tasknum'];?></td>
                    <td><?= $task['title'];?></td>
                    <td><?= $task['description'];?></td>
                    <td><?= $task['maid_name'];?></td>
                    <td><?= $task['owner_name'];?></td>
                    <td><?= $task['status'];?></td>
                    <td><?= $task['taskdate'];?></td>
                    <?php if(isset($_SESSION['role']) && $_SESSION['role'] == 1): ?>
                        <td class="text-center">
                    <form action="<?=$GLOBALS['APP_URL']?>/dashboard/view_tasks.php" method="post">
                    <input type="hidden" name="tasknum" value="<?= $task['tasknum'];?>">
                        <button type="submit" name="delete_task" class="button_trans"><i class="far fa-trash"></i></button>
                        </form>
                    </td>                     
                    <?php elseif(isset($_SESSION['role']) && $_SESSION['role'] == 2): 
                        if($task['status'] == 'pending'):
                        ?>
                        <td class="text-center">
                          <form action="<?=$GLOBALS['APP_URL']?>/dashboard/view_tasks.php" method="post">
                            <input type="hidden" name="tasknum" value="<?= $task['tasknum'];?>">
                            <button type="submit" name="complete_task" class="btn btn-primary">Complete</button>
                        </form>
                    </td> 
                        <?php else: ?>
                           <td class="text-center"><i class="fa fa-check"></i></td> 
                        <?php endif;?>                    
                    <?php endif; ?>
                  
                    </tr>
                    <?php endforeach; ?>
                </tbody>
                </table>
			</div>
	</section>
	<footer id="footer" class="footer"> <?php include '../footer.php';?> </footer>
	
</body>

</html>
