<!DOCTYPE html>
<html lang="en">
<?php include '../head.php';?>

<body>

	<nav class="navbar navbar-expand-lg top_nav">
			<div class="container">
				<a class="navbar-brand" href="#"><img src="../images/logo.jpeg" alt="Logo: Purple True Mark" class="logo"/></a>
				 
				<div class="top_nav_left"> <?php include '../top_nav_left.php';?> </div>
       			<div class="top_nav_right"> <?php include '../top_nav_right.php';?> </div>
			</div>
		</nav>
	<!---about Section Start-->
	<section class="article" id="help" style="min-height:661px;">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
            <?php include('header.php') ?>
				</div>
            </div>
            <div class="container mt-5 ">
            <a href="<?=$GLOBALS['APP_URL']?>/dashboard/add_news.php" class=" col-md-3 nav-background add_new_btn">Add News</a>
            <form action="" class="row col-md-12 mt-2">
				
                <div class="form-group col-md-4">
                    <input type="search" placeholder="Search by News title" name="key_word" id="form1" value="<?= (isset($_GET['key_word']))? $_GET['key_word'] : '';?>" class="form-control " />
                </div>
                <div class="col-md-2">
                    <button type="submit" class="nav-background add_new_btn ">
                        Search by news title <i class="fas fa-search"></i>
                    </button>
                </div>
        </form>
              <table class="table table-stripped nav-background text-white mt-20">
                <thead>
                    <tr>
                    <th scope="col">#</th>
                    <th scope="col">Title</th>
                    <th scope="col">Paragraph</th>
                    <th scope="col">Image</th>
                    <th scope="col">Date</th>
                    <th scope="col" class="text-center">CreatedBy</th>
                    <th scope="col" class="text-center">Edit</th>
                    <th scope="col" class="text-center">Delete</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                    $all_news = $newsController->getNews();
                    foreach($all_news as $news): ?>
                    <tr>
                    <th scope="row"><?= $news['id'];?></th>
                    <td><?= $news['title'];?></td>
                    <td><?= $news['paragraph'];?></td>
                    <td> <img src="<?= $GLOBALS['APP_URL']."/control".$news['image'];?>" alt="" width="60" height="60"> </td>
                    <td><?= $news['date'];?></td>
                    <td class="text-center"><?= $news['username'];?></td>
                    <td class="text-center"><a href="edit_news.php?id=<?= $news['id'];?>" class="text-white "><i class="far fa-edit"></i></a></td>
                    <td class="text-center">
                    <form action="<?=$GLOBALS['APP_URL']?>/dashboard/view_news.php" method="post">
                    <input type="hidden" name="news_id" value="<?= $news['id'];?>">
                        <button type="submit" name="delete_news" class="button_trans"><i class="far fa-trash"></i></button>
                        </form>
                    </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
                </table>
			</div>
	</section>
	<footer id="footer" class="footer"> <?php include '../footer.php';?> </footer>
	
</body>

</html>
