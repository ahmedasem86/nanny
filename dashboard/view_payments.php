<!DOCTYPE html>
<html lang="en">
<?php include '../head.php';?>

<body>

	<nav class="navbar navbar-expand-lg top_nav">
			<div class="container">
				<a class="navbar-brand" href="#"><img src="../images/logo.jpeg" alt="Logo: Purple True Mark" class="logo"/></a>
				 
				<div class="top_nav_left"> <?php include '../top_nav_left.php';?> </div>
       			<div class="top_nav_right"> <?php include '../top_nav_right.php';?> </div>
			</div>
		</nav>
	<!---about Section Start-->
	<section class="article" id="help" style="min-height:661px;">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
            <?php include('header.php');
                $status=['pending' , 'payment confirmed'];
            ?>
				</div>
            </div>
            <div class="container mt-5 ">
                <?php if(isset($_SESSION['role']) && $_SESSION['role'] == 1): ?>
                   <h3>My maids Payments</h3> 
                <?php elseif(isset($_SESSION['role']) && $_SESSION['role'] == 2): ?>
                    <h3> My Work payments</h3>  
                <?php endif;?>
              <table class="table table-stripped nav-background text-white mt-20">
                <thead>
                    <tr>
                    <th scope="col">#</th>
                    <th scope="col">Amount</th>
                    <th scope="col">Payment type</th>
                    <th scope="col">Description</th>
                    <th scope="col">Paid date</th>
                    <th scope="col">Status</th>
                    <th scope="col">Purpose</th>
                    <th scope="col">User</th>
                    <th scope="col">Maid</th>
                    <?php if(isset($_SESSION['role']) && $_SESSION['role'] == 1):?>
                     <th  class="text-center" scope="col">Edit</th>
                     <th  class="text-center" scope="col">Delete</th>
                     <?php elseif(isset($_SESSION['role']) && $_SESSION['role'] == 2): ?>
                      <th class="text-center" scope="col">Approve payment</th>
                     <?php endif; ?>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                    $payments = $paymentController->getPayments();
                    if($payments):
                    foreach($payments as $payment): ?>
                    <tr>
                    <td><?= $payment['paymentId'];?></td>
                    <td><?= $payment['amount'];?></td>
                    <td><?= $payment['type'];?></td>
                    <td><?= $payment['description'];?></td>
                    <td><?= $payment['paidDate'];?></td>
                    <td><?= $status[$payment['status']];?></td>
                    <td><?= $payment['purpose'];?></td>
                    <td><?= $payment['owner_name'];?></td>
                    <td><?= $payment['maid_name'];?></td>

                    <?php if(isset($_SESSION['role']) && $_SESSION['role'] == 1):
                            if($payment['status'] == 0): ?>
                    <td class="text-center">
                   <a href="edit_payment.php?id=<?= $payment['paymentId']?>"><i class="far fa-edit"></i></a>
                    </td> 
                        <?php
                            else:
                                ?>
                                <td class="text-center" style="font-wieght:bold; border:1px solid white;"> Cant edit approved payment</td>
                        <?php
                            endif;
                        ?>

                    <td class="text-center">
                    <form action="<?=$GLOBALS['APP_URL']?>/dashboard/view_payments.php" method="post">
                    <input type="hidden" name="paymentId" value="<?= $payment['paymentId'];?>">
                        <button type="submit" name="delete_payment" class="button_trans"><i class="far fa-trash"></i></button>
                        </form>
                    </td>                     
                    <?php elseif(isset($_SESSION['role']) && $_SESSION['role'] == 2): 
                        if($payment['status'] == '0'):
                        ?>
                        <td class="text-center">
                          <form action="<?=$GLOBALS['APP_URL']?>/dashboard/view_payments.php" method="post">
                            <input type="hidden" name="paymentId" value="<?= $payment['paymentId'];?>">
                            <button type="submit" name="approve_payment" class="btn btn-primary">Approve Payment</button>
                        </form>
                    </td> 
                        <?php else: ?>
                           <td class="text-center"><i class="fa fa-check"></i></td> 
                        <?php endif;?>                    
                    <?php endif; ?>
                  
                    </tr>
                    <?php endforeach; endif;?>
                    
                </tbody>
                </table>
			</div>
	</section>
	<footer id="footer" class="footer"> <?php include '../footer.php';?> </footer>
	
</body>

</html>
