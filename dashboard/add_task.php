<!DOCTYPE html>
<html lang="en">
<?php include '../head.php';?>

<body>

	<nav class="navbar navbar-expand-lg top_nav">
			<div class="container">
				<a class="navbar-brand" href="#"><img src="../images/logo.jpeg" alt="Logo: Purple True Mark" class="logo"/></a>
				 
				<div class="top_nav_left"> <?php include '../top_nav_left.php';?> </div>
       			<div class="top_nav_right"> <?php include '../top_nav_right.php';?> </div>
			</div>
		</nav>
	<!---about Section Start-->
	<section class="article" id="help" style="min-height:661px;">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
            <?php include('header.php') ?>
				</div>
            </div>
            <div class="container mt-5 ">
                <h4>Add Task</h4>
                <br>
            	<form action="<?=$GLOBALS['APP_URL']?>/dashboard/view_tasks.php" method="post" style="padding-left:420px; min-height:670px;" enctype="multipart/form-data">
			<table>
			<tr>	<th>Title: </th>	<td><input  class="form-control" type="text" name="title" size ="30" maxlength="50" required></td>	</tr>
            <tr>	<th>Task Deadline: </th>	<td><input type="datetime-local" name="taskdate" required></td>	</tr>		
            <tr>	<th>Description: </th>	<td><textarea class="form-control" name="description" id="" cols="30" rows="10" required></textarea></td>	</tr>		 
		
            <input type="hidden" name="userId" value="<?= $_GET['owner_id']?>">
            <input type="hidden" name="maidId" value="<?= $_GET['maid_id']?>">
            <input type="hidden" name="status" value="pending">
            <tr> <td colspan="2"><input type="submit"  class="form-control" name="addtask" value ="Add task"></td> </tr>
			</table>
			</div>
	</section>
	<footer id="footer" class="footer"> <?php include '../footer.php';?> </footer>
	
</body>

</html>
