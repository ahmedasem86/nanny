<!DOCTYPE html>
<html lang="en">
<?php include '../head.php';?>

<body>

	<nav class="navbar navbar-expand-lg top_nav">
			<div class="container">
				<a class="navbar-brand" href="#"><img src="../images/logo.jpeg" alt="Logo: Purple True Mark" class="logo"/></a>
				 
				<div class="top_nav_left"> <?php include '../top_nav_left.php';?> </div>
       			<div class="top_nav_right"> <?php include '../top_nav_right.php';?> </div>
			</div>
		</nav>
	<!---about Section Start-->
	<section class="article" id="help" style="min-height:661px;">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
            <?php include('header.php') ?>
				</div>
            </div>
            <div class="container mt-5 ">
            <a href="<?=$GLOBALS['APP_URL']?>/dashboard/add_user.php" class=" col-md-3 nav-background add_new_btn">Add New Maid</a>

              <table class="table table-stripped nav-background text-white mt-20">
                <thead>
                    <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">User name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Phone</th>
                    <th scope="col">Address</th>
                    <th scope="col">Edit</th>
                    <th scope="col">Delete</th>
                    <th scope="col">Add task</th>
                    <th scope="col">Pay</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                    $users = $authController->getUsers();
                    foreach($users as $user): ?>
                    <tr>
                    <th scope="row"><?= $user['userId'];?></th>
                    <td><?= $user['fullname'];?></td>
                    <td><?= $user['username'];?></td>
                    <td><?= $user['email'];?></td>
                    <td><?= $user['phone'];?></td>
                    <td><?= $user['address'];?></td>
                    <td class="text-center"><a href="edit_user.php?id=<?= $user['userId'];?>" class="text-white "><i class="far fa-edit"></i></a></td>
                    <td class="text-center">
                    <form action="<?=$GLOBALS['APP_URL']?>/dashboard/view_users.php" method="post">
                    <input type="hidden" name="user_id" value="<?= $user['userId'];?>">
                        <button type="submit" name="delete_user" class="button_trans"><i class="far fa-trash"></i></button>
                        </form>
                    </td>
                    <?php if(isset($_SESSION['role']) && $_SESSION['role'] == 1): ?>
                      <td class="text-center">
                      <form action="<?=$GLOBALS['APP_URL']?>/dashboard/add_task.php" method="get">
                          <input type="hidden" name="maid_id" value="<?= $user['userId'];?>">
                          <input type="hidden" name="owner_id" value="<?= $_SESSION['id'];?>">
                         <button type="submit" class="button_trans"><i class="far fa-plus"></i></button>
                        </form> 
                        </td>                       
                     <?php endif; ?>
                     <?php if(isset($_SESSION['role']) && $_SESSION['role'] == 1): ?>
                      <td class="text-center">
                      <form action="<?=$GLOBALS['APP_URL']?>/dashboard/add_payment.php" method="get">
                          <input type="hidden" name="maid_id" value="<?= $user['userId'];?>">
                          <input type="hidden" name="owner_id" value="<?= $_SESSION['id'];?>">
                         <button type="submit" class="button_trans"><i class="far fa-plus"></i></button>
                        </form> 
                        </td>                       
                     <?php endif; ?>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
                </table>
			</div>
	</section>
	<footer id="footer" class="footer"> <?php include '../footer.php';?> </footer>
	
</body>

</html>
