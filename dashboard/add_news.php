<!DOCTYPE html>
<html lang="en">
<?php include '../head.php';?>

<body>

	<nav class="navbar navbar-expand-lg top_nav">
			<div class="container">
				<a class="navbar-brand" href="#"><img src="../images/logo.jpeg" alt="Logo: Purple True Mark" class="logo"/></a>
				 
				<div class="top_nav_left"> <?php include '../top_nav_left.php';?> </div>
       			<div class="top_nav_right"> <?php include '../top_nav_right.php';?> </div>
			</div>
		</nav>
	<!---about Section Start-->
	<section class="article" id="help" style="min-height:661px;">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
            <?php include('header.php') ?>
				</div>
            </div>
            <div class="container mt-5 ">
                <h4>Add News</h4>
                <br>
            	<form action="<?=$GLOBALS['APP_URL']?>/dashboard/view_news.php" method="post" style="padding-left:420px; min-height:670px;" enctype="multipart/form-data">
			<table>
			<tr>	<th>Title: </th>	<td><input  class="form-control" type="text" name="title" size ="30" maxlength="50" required></td>	</tr>
			<tr>	<th>Paragraph: </th>	<td><textarea  class="form-control"  name="paragraph" required ></textarea></td>	</tr>
			<tr>	<th>Image: </th>	<td><input  class="form-control" type="file" name="image" required></td>	</tr>
			<tr>	<th>Date: </th>	<td><input  class="form-control" type="date" name="date" required></td>	</tr>
			<!-- <tr>	<th>Author: </th>	<td><input  class="form-control" type="text" name="address" size ="30" maxlength="100"></td>	</tr> -->
			<input type="hidden" name="author" value="<?= $_SESSION['id'] ?>">
            <tr> <td colspan="2"><input type="submit"  class="form-control" name="add_news" value ="Add news"></td> </tr>
			</table>
			</form>
			</div>
	</section>
	<footer id="footer" class="footer"> <?php include '../footer.php';?> </footer>
	
</body>

</html>
