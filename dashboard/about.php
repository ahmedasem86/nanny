<!DOCTYPE html>
<html lang="en">
<?php include '../head.php';?>

<body>

	<nav class="navbar navbar-expand-lg top_nav">
			<div class="container">
				<a class="navbar-brand" href="#"><img src="../images/logo.jpeg" alt="Logo: Purple True Mark" class="logo"/></a>
				 
				<div class="top_nav_left"> <?php include '../top_nav_left.php';?> </div>
       			<div class="top_nav_right"> <?php include '../top_nav_right.php';?> </div>
			</div>
		</nav>
	<!---about Section Start-->
	<section class="article" id="help" style="min-height:661px;">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
            <?php include('header.php');
                $about =   $aboutController->getAbout();
            ?>
				</div>
            </div>
            <div class="container mt-5 ">
                <h4>About us page</h4>
                <br>
            	<form action="<?=$GLOBALS['APP_URL']?>/dashboard/about.php" method="post" style=" min-height:670px;" enctype="multipart/form-data">
			<table>
			<tr>	<th>About paragraph : </th>	<td><textarea  class="form-control" cols="100" rows="10" style="margin-bottom:10px;" name="paragraph"  required><?= $about['paragraph']?> </textarea></td>	</tr>
		            <tr> <td colspan="2"><input type="submit"  class="form-control" name="update_about" value ="Update paragraph"></td> </tr>
			</table>
			</div>
	</section>
	<footer id="footer" class="footer"> <?php include '../footer.php';?> </footer>
	
</body>

</html>
