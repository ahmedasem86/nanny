<!DOCTYPE html>
<html lang="en">
<?php include '../head.php';?>

<body>

	<nav class="navbar navbar-expand-lg top_nav">
			<div class="container">
				<a class="navbar-brand" href="#"><img src="../images/logo.jpeg" alt="Logo: Purple True Mark" class="logo"/></a>
				 
				<div class="top_nav_left"> <?php include '../top_nav_left.php';?> </div>
       			<div class="top_nav_right"> <?php include '../top_nav_right.php';?> </div>
			</div>
		</nav>
	<!---about Section Start-->
	<section class="article" id="help" style="min-height:661px;">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
            <?php include('header.php') ?>
				</div>
            </div>
            <div class="container mt-5 ">
                <h4>Add user</h4>
                <br>
            	<form action="<?=$GLOBALS['APP_URL']?>/dashboard/view_users.php" method="post" style="padding-left:420px; min-height:670px;" enctype="multipart/form-data">
			<table>
			<tr>	<th>User name: </th>	<td><input  class="form-control" type="text" name="username" size ="30" maxlength="50" required></td>	</tr>
			<tr>	<th>Full name: </th>	<td><input  class="form-control" type="text" name="fullname" size ="30" maxlength="50"required></td>	</tr>
			<tr>	<th>Email: </th>	<td><input  class="form-control" type="email" name="email" size ="30" maxlength="100"required></td>	</tr>
			<tr>	<th>Phone: </th>	<td><input  class="form-control" type="text" name="phone" size ="30" maxlength="30"required></td>	</tr>
			<tr>	<th>Address: </th>	<td><input  class="form-control" type="text" name="address" size ="30" maxlength="100"required></td>	</tr>
			<tr>	<th>Password: </th>	<td><input   class="form-control" type="password" name="password" size ="30" maxlength="30"required></td>	</tr>
			<tr>	<th>Confirm Password: </th>	<td><input  class="form-control" type="password" name="password_confirmation" size ="30" maxlength="30"required></td>	</tr>
			<input type="hidden" name="by_admin" value="byadmin">
            <input type="hidden" name="parent_id" value="<?= $_SESSION['id']?>">
            <input type="hidden" name="role" value="2">

            <tr> <td colspan="2"><input type="submit"  class="form-control" name="register" value ="Register"></td> </tr>
			</table>
			</div>
	</section>
	<footer id="footer" class="footer"> <?php include '../footer.php';?> </footer>
	
</body>

</html>
