<!DOCTYPE html>
<html lang="en">
<?php include '../head.php';?>


<body>

	<nav class="navbar navbar-expand-lg top_nav">
			<div class="container">
				<a class="navbar-brand" href="#"><img src="../images/logo.jpeg" alt="Logo: Purple True Mark" class="logo"/></a>
				 
				<div class="top_nav_left"> <?php include '../top_nav_left.php';?> </div>
       			<div class="top_nav_right"> <?php include '../top_nav_right.php';?> </div>
			</div>
		</nav>
	<!---about Section Start-->
	<section class="article" id="help" style="min-height:661px;">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
            <?php include('header.php') ?>
				</div>
				<h4 class="mt-20"> Welcome <?= $_SESSION['fullname']?> to dashboard</h4>
		</div>
	</section>

	<footer id="footer" class="footer"> <?php include '../footer.php';?> </footer>
	
</body>

</html>
