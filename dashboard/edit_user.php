<!DOCTYPE html>
<html lang="en">
<?php include '../head.php';?>

<body>

	<nav class="navbar navbar-expand-lg top_nav">
			<div class="container">
				<a class="navbar-brand" href="#"><img src="../images/logo.jpeg" alt="Logo: Purple True Mark" class="logo"/></a>
				 
				<div class="top_nav_left"> <?php include '../top_nav_left.php';?> </div>
       			<div class="top_nav_right"> <?php include '../top_nav_right.php';?> </div>
			</div>
		</nav>
	<!---about Section Start-->
	<section class="article" id="help" style="min-height:661px;">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
            <?php include('header.php') ?>
				</div>
            </div>
            <?php   $user = $authController->getUserById($_GET['id']);?>

            <div class="container mt-5 ">
                <h4>Editing User <span class="color_primary"><?= $user['username'];?></span></h4>
                <br>
                <form class="row" action="<?=$GLOBALS['APP_URL']?>/dashboard/view_users.php" method="post">
                    <div class="form-group col-md-6">
                        <label >Full name</label>
                        <input type="text" class="form-control"  name="fullname" placeholder="Enter Full name" value="<?= $user['fullname']; ?>" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label >Email</label>
                        <input type="email" class="form-control"  name="email" placeholder="Enter Email" value="<?= $user['email'];?>" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label >Address</label>
                        <input type="text" class="form-control"  name="address" placeholder="Enter user address" value="<?= $user['address'];?>" required>
                    </div>
                    <input type="hidden" name="user_id" value="<?= $user['userId'];?>">
                    <div class="form-group col-md-6">
                     <button type="submit" name="user_update" class="submit_form col-md-12 nav-background">Submit</button>
                    </div>
                </form>
			</div>
	</section>
	<footer id="footer" class="footer"> <?php include '../footer.php';?> </footer>
	
</body>

</html>
