<style>
  
.active-dashboard{
    background: wheat;
    color: #a04b43 !important;
}

</style>
<?php if(isset($_SESSION['id']) && $_SESSION['role'] == 3): ?>
<nav class="navbar navbar-expand-lg navbar-light nav-background">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav mr-auto">
                <li class="nav-item ">
                  <a class="nav-link <?= ($_SERVER['REQUEST_URI'] == $GLOBALS['APP_URL']."/dashboard/index.php")? 'active-dashboard' : '';?>" href="index.php">Home</a>
                </li>
                <li class="nav-item ">
                  <a class="nav-link <?= ($_SERVER['REQUEST_URI'] == $GLOBALS['APP_URL']."/dashboard/view_users.php")? 'active-dashboard' : '';?>" href="view_users.php">Users</a>
                </li>
                <li class="nav-item ">
                  <a class="nav-link <?= ($_SERVER['REQUEST_URI'] == $GLOBALS['APP_URL']."/dashboard/view_news.php")? 'active-dashboard' : '';?>" href="view_news.php">News</a>
                </li>
                <li class="nav-item ">
                  <a class="nav-link <?= ($_SERVER['REQUEST_URI'] == $GLOBALS['APP_URL']."/dashboard/view_tasks.php")? 'active-dashboard' : '';?>" href="view_tasks.php">Tasks</a>
                </li>
                <li class="nav-item dropdown">
                  <a class="nav-link <?= ($_SERVER['REQUEST_URI'] == $GLOBALS['APP_URL']."/dashboard/view_payment_types.php")? 'active-dashboard' : '';?> dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Payments
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item " href="<?=$GLOBALS['APP_URL']?>/dashboard/view_payment_types.php">Payment Type</a>
                  </div>
                </li>
                <li class="nav-item">
                  <a class="nav-link <?= ($_SERVER['REQUEST_URI'] == $GLOBALS['APP_URL']."/dashboard/about.php")? 'active-dashboard' : '';?>" href="about.php">About</a>
                </li>
              </ul>
            </div>
          </nav>
          <?php elseif(isset($_SESSION['id']) && $_SESSION['role'] == 1): ?>
            <nav class="navbar navbar-expand-lg navbar-light nav-background">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                  <a class="nav-link <?= ($_SERVER['REQUEST_URI'] == $GLOBALS['APP_URL']."/dashboard/index.php")? 'active-dashboard' : '';?>" href="index.php">Home</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link <?= ($_SERVER['REQUEST_URI'] == $GLOBALS['APP_URL']."/dashboard/view_maids.php")? 'active-dashboard' : '';?>" href="view_maids.php">My Maids</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link <?= ($_SERVER['REQUEST_URI'] == $GLOBALS['APP_URL']."/dashboard/view_tasks.php")? 'active-dashboard' : '';?>" href="view_tasks.php">Tasks</a>
                </li>
             
                <!-- <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Payments
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="<?=$GLOBALS['APP_URL']?>/dashboard/view_payment_types.php">Payment Type</a>
                  </div>
                </li> -->
                <li class="nav-item">
                  <a class="nav-link <?= ($_SERVER['REQUEST_URI'] == $GLOBALS['APP_URL']."/dashboard/view_payments.php")? 'active-dashboard' : '';?>" href="view_payments.php">My Payments</a>
                </li>
              </ul>
            </div>
          </nav>
          <?php elseif(isset($_SESSION['id']) && $_SESSION['role'] == 2):?>
            <nav class="navbar navbar-expand-lg navbar-light nav-background">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                  <a class="nav-link <?= ($_SERVER['REQUEST_URI'] == $GLOBALS['APP_URL']."/dashboard/index.php")? 'active-dashboard' : '';?>" href="index.php">Home</a>
                </li>

                <li class="nav-item">
                  <a class="nav-link <?= ($_SERVER['REQUEST_URI'] == $GLOBALS['APP_URL']."/dashboard/view_tasks.php")? 'active-dashboard' : '';?>" href="view_tasks.php">My tasks</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link <?= ($_SERVER['REQUEST_URI'] == $GLOBALS['APP_URL']."/dashboard/view_payments.php")? 'active-dashboard' : '';?>" href="view_payments.php">My Payments</a>
                </li>
              </ul>
            </div>
          </nav>
            <?php endif;?>
