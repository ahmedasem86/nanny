<?php

require_once 'Connection.php';



$newsController = new NewsControl();

if(isset($_REQUEST["get_news"])) $newsController->getNews();
if(isset($_REQUEST["add_news"])) $newsController->addNews();
if(isset($_REQUEST["update_news"])) $newsController->updateNews();
if(isset($_REQUEST["delete_news"])) $newsController->deleteNews();




class NewsControl{


    private $dbConnection; 
    
    private $conn;
    
    public function __construct(){
        $this->dbConnection = new Connection();
        $this->conn = $this->dbConnection->getConn();
    }

    function getNews()
    {
        $key_word = (isset($_GET['key_word']))? $_GET['key_word'] : '';

        $sql_news_query = "Select news.id , news.title , news.paragraph , news.image , news.date , users.username FROM news INNER JOIN users ON news.userId=users.userId WHERE news.title  like '%".$key_word."%' ";
        if(!$news = $this->conn->query($sql_news_query)) {
            $_SESSION["Error_MSG"] = "An Error happened while processing your retrive request! 2";
        }
        else 
        {
            $_SESSION["Error_MSG"]=[];
            return $news ;            
        }
     }
     function addNews(){
        $title =(!empty($_POST['title']))? $_POST['title'] : '';
        $paragraph =(!empty($_POST['paragraph']))? $_POST['paragraph'] : '';
        $date =(!empty($_POST['date']))? $_POST['date'] : '';
        $author =(!empty($_POST['author']))? $_POST['author'] : '';
        $image = '';

        if(!empty($_FILES["image"]['name'])){
            define ('SITE_ROOT', realpath(dirname(__FILE__)));
    
            $uploaddir = '/images/news/';
            $uploadfile = $uploaddir . basename($_FILES['image']['name']);
    
            echo "<p>";
            $file_upload = move_uploaded_file($_FILES['image']['tmp_name'], SITE_ROOT.$uploadfile);
            if ($file_upload) {
                $image = $uploadfile;
            } else {
              array_push($errors , 'news image uploading failed'); 
            }
        }
            $insert_news_query = "INSERT INTO news (title ,paragraph , date , userId ,image) VALUES ('$title','$paragraph' , '$date' , '$author' , '$image')";             
            $result_of_the_query = $this->conn->query($insert_news_query);             
            if($result_of_the_query == false){
                echo $this->conn->error;
                die();
            }             
     }
     function getnewsById($id){
        $newsById = "Select news.id , news.title , news.paragraph , news.image , news.date , users.username FROM news INNER JOIN users ON news.userId=users.userId Where id=$id LIMIT 1 ";
        $news = $this->conn->query($newsById)->fetch_assoc();
        return $news;
     }
     function updateNews(){
        $title =(!empty($_POST['title']))? $_POST['title'] : '';
        $paragraph =(!empty($_POST['paragraph']))? $_POST['paragraph'] : '';
        $date =(!empty($_POST['date']))? $_POST['date'] : '';
        $image = '';
        $news_id = (!empty($_POST['news_id']))? $_POST['news_id'] : '';
        if(!empty($_FILES["image"]['name'])){

            define ('SITE_ROOT', realpath(dirname(__FILE__)));
    
            $uploaddir = '/images/news/';
            $uploadfile = $uploaddir . basename($_FILES['image']['name']);
    
            echo "<p>";
            $file_upload = move_uploaded_file($_FILES['image']['tmp_name'], SITE_ROOT.$uploadfile);
            if ($file_upload) {
                $image = $uploadfile;
            } else {
              array_push($errors , 'news image uploading failed'); 
            }
        }
        if(!empty($image)):
        $update_news_query = "UPDATE news SET  title = '$title' , paragraph = '$paragraph' , image ='$image', date = '$date' Where id = $news_id";
        else:
            $update_news_query = "UPDATE news SET  title = '$title' , paragraph = '$paragraph' , date = '$date' Where id = $news_id";
        endif;
        $news = $this->conn->query($update_news_query);  
     }
     function deleteNews(){
        $id =(!empty($_POST['news_id']))? $_POST['news_id'] : '';
        $delete_news_query = "DELETE FROM news WHERE id=$id";
        $payment_news_delete = $this->conn->query($delete_news_query);
     }
}
?>