<?php
    $GLOBALS['APP_URL'] = '';
    
class Connection 
{
    // The DB Connection variable
    private $conn;
    
    /**
     * 
     * DB Connection parameteres
     */
    private $dbhost = "localhost";
    private $dbuser = "root";
    private $dbpass = "";
    private $db = "nanny";
    
    public function __construct(){
        $this->conn = new mysqli($this->dbhost, $this->dbuser, $this->dbpass, $this->db) or die("Connect failed: %s\n". $this->conn -> error);
    }

    public function getConn()
    {
        return $this->conn;
    }

}

?>