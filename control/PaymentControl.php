<?php

require_once 'Connection.php';

$paymentController = new PaymentControl();

if(isset($_REQUEST["get_payment_type"])) $paymentController->getPaymentType();
if(isset($_REQUEST["add_payment_type"])) $paymentController->addPaymentType();
if(isset($_REQUEST["payment_update"])) $paymentController->updatePaymentType();
if(isset($_REQUEST["delete_payment_type"])) $paymentController->deletePaymentType();
if(isset($_REQUEST["get_payments"])) $paymentController->getPayments();
if(isset($_REQUEST["update_payment"])) $paymentController->updatePayment();
if(isset($_REQUEST["addPayment"])) $paymentController->addPayment();
if(isset($_REQUEST["delete_payment"])) $paymentController->deletePayment();
if(isset($_REQUEST["approve_payment"])) $paymentController->approvePayment();





class PaymentControl{


    private $dbConnection; 
    
    private $conn;
    
    public function __construct(){
        $this->dbConnection = new Connection();
        $this->conn = $this->dbConnection->getConn();
        
        // $sql = "SELECT * FROM aboutus;";
        // $result = $this->conn->query($sql);
        
        // if (!$result->num_rows > 0)
        // {
        //     $sql = "INSERT INTO aboutus (id, paragraph) VALUES (1, 'this is standard paragraph'); ";
        //     $this->conn->query($sql);
        // } 
    }

    function getPaymentType()
    {

        $payment_type_query = "Select * FROM paymenttype;";
        if(!$pay_type = $this->conn->query($payment_type_query)) {
            $_SESSION["Error_MSG"] = "An Error happened while processing your retrive request! 2";
            header('Location: ../index.php');
        }
        else 
        {
            $_SESSION["Error_MSG"]=[];
            return $pay_type;             
        }
     }

     function addPaymentType(){
        $type =(!empty($_POST['type']))? $_POST['type'] : '';
        $insert_payment_type_query = "INSERT INTO paymenttype (type) VALUES ('$type')";             
        $result_of_the_query = $this->conn->query($insert_payment_type_query);             
        if($result_of_the_query == false){
            echo $this->conn->error;
            die();
        }
     }

     function getPaymentTypeById($id){
        $paymentTypeById = "SELECT * FROM paymenttype Where id=$id LIMIT 1 ";
        $payment_type = $this->conn->query($paymentTypeById)->fetch_assoc();
        return $payment_type;
     }

     function updatePaymentType(){
        $type =(!empty($_POST['type']))? $_POST['type'] : '';
        $id =(!empty($_POST['type_id']))? $_POST['type_id'] : '';
        $update_payment_type_query = "UPDATE paymenttype SET  type = '$type'  Where id = $id";
        $payment_type = $this->conn->query($update_payment_type_query);
     }
     function deletePaymentType(){
        $id =(!empty($_POST['type_id']))? $_POST['type_id'] : '';
        $delete_paymenttype_query = "DELETE FROM paymenttype WHERE id=$id";
        $payment_type_delete = $this->conn->query($delete_paymenttype_query);
     }
     function getPayments(){
        if($_SESSION['role'] == 1){
            $userId = $_SESSION['id'];
            										
           $get_all_payments = "SELECT  payments.paymentId , payments.amount , paymenttype.type ,payments.description , payments.paidDate , payments.status , payments.purpose , owner.username as owner_name  ,maid.username as maid_name  , paymenttype.type as payment_type    FROM payments Inner join users As owner on payments.userId = owner.userId Inner join users AS maid on payments.maidId = maid.userId  Inner join paymenttype on payments.paymenttype = paymenttype.id Where payments.userId = $userId ";
        }elseif($_SESSION['role'] == 2){
            $maidId =$_SESSION['id'];
            $get_all_payments = "SELECT  payments.paymentId , payments.amount , paymenttype.type ,payments.description , payments.paidDate , payments.status , payments.purpose , owner.username as owner_name  ,maid.username as maid_name  , paymenttype.type as payment_type    FROM payments Inner join users As owner on payments.userId = owner.userId Inner join users AS maid on payments.maidId = maid.userId Inner join paymenttype on payments.paymenttype = paymenttype.id Where payments.maidId = $maidId ";

        }else{
            $get_all_payments = "SELECT  payments.paymentId , payments.amount , paymenttype.type ,payments.description , payments.paidDate , payments.status , payments.purpose , owner.username as owner_name  ,maid.username as maid_name  , paymenttype.type as payment_type    FROM payments Inner join users As owner on payments.userId = owner.userId Inner join users AS maid on payments.maidId = maid.userId Inner join paymenttype on payments.paymenttype = paymenttype.id ";

        }
       $payments = $this->conn->query($get_all_payments);
       return $payments;
     }
     function getPaymentById($id){
        $paymentById = "SELECT * FROM payments Where paymentId=$id LIMIT 1 ";
        $payment = $this->conn->query($paymentById)->fetch_assoc();
        return $payment;
     }
     function addPayment(){
        $purpose =(!empty($_POST['purpose']))? $_POST['purpose'] : '';
        $description =(!empty($_POST['description']))? $_POST['description'] : '';
        $paidDate = (!empty($_POST['paidDate']))?$_POST['paidDate']:date("Y/m/d");
        $amount = (!empty($_POST['amount']))?$_POST['amount']:date("Y/m/d");
        $status =(!empty($_POST['status']))? $_POST['status'] : '0';
        $userId =(!empty($_POST['userId']))? $_POST['userId'] : '';
        $maidId =(!empty($_POST['maidId']))? $_POST['maidId'] : '';
        $paymenttype =(!empty($_POST['paymenttype']))? $_POST['paymenttype'] : 0;

        $insert_payment_query = "INSERT INTO payments (purpose , description , paiddate ,amount, status , userId  , maidId , paymenttype)
         VALUES ('$purpose' , '$description' , '$paidDate' ,'$amount', '$status' , '$userId'  , '$maidId' , '$paymenttype')";             
        $result_of_the_query = $this->conn->query($insert_payment_query);  
        
        if($result_of_the_query == false){
            echo $this->conn->error;
            die();
        }
     }
     function updatePayment(){
        $payment_id =(!empty($_POST['payment_id']))? $_POST['payment_id'] : '';
        $purpose =(!empty($_POST['purpose']))? $_POST['purpose'] : '';
        $description =(!empty($_POST['description']))? $_POST['description'] : '';
        $amount = (!empty($_POST['amount']))?$_POST['amount']:date("Y/m/d");
        $paymenttype =(!empty($_POST['paymenttype']))? $_POST['paymenttype'] : 0;
        $update_payment_query = "UPDATE payments SET  purpose = '$purpose' , description = '$description' , amount = '$amount' , paymenttype = '$paymenttype'    Where paymentId = $payment_id";

        $payment = $this->conn->query($update_payment_query);
     }
     function deletePayment(){
        $paymentId =(!empty($_POST['paymentId']))? $_POST['paymentId'] : '';
        $delete_payment_query = "DELETE FROM payments WHERE paymentId=$paymentId";
        $payment_delete = $this->conn->query($delete_payment_query);
     }
     function approvePayment(){
        $paymentId =(!empty($_POST['paymentId']))? $_POST['paymentId'] : '';
        $confirm_payment_query = "UPDATE payments SET  status = '1'  Where paymentId = $paymentId";
        $confirm_Task = $this->conn->query($confirm_payment_query);
     }
}
?>