<?php

require_once 'Connection.php';



$taskController = new TaskControl();

if(isset($_REQUEST["get_tasks"])) $taskController->getTasks();
if(isset($_REQUEST["addtask"])) $taskController->addTask();
if(isset($_REQUEST["delete_task"])) $taskController->deleteTask();
if(isset($_REQUEST["complete_task"])) $taskController->completeTask();

class TaskControl{


    private $dbConnection; 
    
    private $conn;
    
    public function __construct(){
        $this->dbConnection = new Connection();
        $this->conn = $this->dbConnection->getConn();
    }

    function getTasks(){
        $key_word = (isset($_GET['key_word']))? $_GET['key_word'] : '';
        if($_SESSION['role'] == 1){
            $userId = $_SESSION['id'];
           $get_all_tasks = "SELECT  task.tasknum , task.title , task.description ,task.taskdate , task.status , owner.username as owner_name , maid.username as maid_name  FROM task inner join users As owner on task.userId = owner.userId inner join users AS maid on task.maidId = maid.userId Where task.userId = $userId && (task.title like '%".$key_word."%' )";
        }elseif($_SESSION['role'] == 2){
            $maidId =$_SESSION['id'];
            $get_all_tasks = "SELECT  task.tasknum , task.title , task.description ,task.taskdate , task.status , owner.username as owner_name , maid.username as maid_name  FROM task inner join users As owner on task.userId = owner.userId inner join users AS maid on task.maidId = maid.userId Where task.maidId = $maidId && (task.title  like '%".$key_word."%' )";

        }else{
            $get_all_tasks = "SELECT  task.tasknum , task.title , task.description ,task.taskdate , task.status , owner.username as owner_name , maid.username as maid_name  FROM task inner join users As owner on task.userId = owner.userId inner join users AS maid on task.maidId = maid.userId WHERE task.title  like '%".$key_word."%' ";

        }
       $tasks = $this->conn->query($get_all_tasks);
       return $tasks;
    }

    function addTask(){
        $title =(!empty($_POST['title']))? $_POST['title'] : '';
        $description =(!empty($_POST['description']))? $_POST['description'] : '';
        $taskdate = (!empty($_POST['taskdate']))?$_POST['taskdate']:date("Y/m/d");
        $status =(!empty($_POST['status']))? $_POST['status'] : 'pending';
        $userId =(!empty($_POST['userId']))? $_POST['userId'] : '';
        $maidId =(!empty($_POST['maidId']))? $_POST['maidId'] : '';

        $insert_task_query = "INSERT INTO task (title , description , taskdate , status , userId  , maidId) VALUES ('$title' , '$description' , '$taskdate' , '$status' , '$userId'  , '$maidId')";             
        $result_of_the_query = $this->conn->query($insert_task_query);  
        
        if($result_of_the_query == false){
            echo $this->conn->error;
            die();
        }
     }
     function deleteTask(){
        $task_id =(!empty($_POST['tasknum']))? $_POST['tasknum'] : '';
        $delete_task_query = "DELETE FROM task WHERE tasknum=$task_id";
        $task_delete = $this->conn->query($delete_task_query);
     }
     
     function completeTask(){
        $tasknum =(!empty($_POST['tasknum']))? $_POST['tasknum'] : '';
        $complete_task_query = "UPDATE task SET  status = 'completed'  Where tasknum = $tasknum";
        $complete_task = $this->conn->query($complete_task_query);
     }
}
?>