<?php

require_once 'Connection.php';



$authController = new AuthControl();

if(isset($_POST['register'])) $authController->registerUser();
if(isset($_POST['login'])) $authController->login();
if(isset($_POST['user_update'])) $authController->userUpdate();
if(isset($_POST['delete_user'])) $authController->deleteUser();
if(isset($_POST['unblock_user'])) $authController->unblockUser();
if(isset($_POST['block_user'])) $authController->blockUser();


class AuthControl{

    private $dbConnection; 
    
    private $conn;
    
    public function __construct(){
        
        $this->dbConnection = new Connection();
        $this->conn = $this->dbConnection->getConn();
        
        $sql = "SELECT * FROM users;";
        $result = $this->conn->query($sql);
        
        if (!$result->num_rows > 0)
        {
            $password = md5('1234');
            $sql = "INSERT INTO users (	fullname ,username , password, email ,phone	,role,address	) VALUES ('Admin', 'admin' ,$password ,'admin@nanny.com','96512345678' ,0,'21215 Safat 13073 Kuwait City'); ";
            $this->conn->query($sql);
        } 
    }
    
    function registerUser(){
            $username =(!empty($_POST['username']))? $_POST['username'] : '';
            $role =(!empty($_POST['role']))? $_POST['role'] : 1;
            $fullname =(!empty($_POST['fullname']))? $_POST['fullname'] : '';
            $email =(!empty($_POST['email']))? $_POST['email'] : '';
            $phone =(!empty($_POST['phone']))? $_POST['phone'] : '';
            $address =(!empty($_POST['address']))? $_POST['address'] : '';
            $password = (!empty($_POST['password']))? $_POST['password']: '';
            $password_confirmation = (!empty($_POST['password_confirmation']))? $_POST['password_confirmation'] : '';
            $is_parent = ($role == 1)? 1 : 0 ;
            $parent_id = (!empty($_POST['parent_id']))? $_POST['parent_id'] : 2;
            //form validation
            if(empty($username)){ array_push($GLOBALS['errors'] , " user name is required");}
            if(empty($fullname)){ array_push($GLOBALS['errors'] , " Full name is required");}
            if(empty($email)){ array_push($GLOBALS['errors'] , "email is required");}
            if(empty($phone)){ array_push($GLOBALS['errors'] , " user phone is required");}
            if(empty($address)){ array_push($GLOBALS['errors'] , " user address is required");}
            if(empty($password)){ array_push($GLOBALS['errors'] , "password is required");}
            if($password != $password_confirmation){ array_push($GLOBALS['errors'] , "Password should match password confirmation");}

            //check db for checking existing username
            $user_check_query = "SELECT * FROM users WHERE username = '$username' or email = '$email' LIMIT 1";
            $result = $this->conn->query($user_check_query);
            if ($result->num_rows > 0){
                $user = $result->fetch_assoc();
                if($user['username'] === $username){ array_push($GLOBALS['errors'] , 'User name already exists'); }
                if($user['email'] === $email){ array_push($GLOBALS['errors'] , 'Email already exists'); }
            }
            // register the user if no errors exist
            if(count($GLOBALS['errors']) == 0){
    
                $password = md5($password);
                if(isset($_POST['parent_id'])):
                $insert_user_query = "INSERT INTO users (username ,fullname , email , phone ,role , address ,  password , is_parent, parent_id) VALUES ('$username','$fullname' , '$email' , '$phone' , '$role' , '$address' , '$password','$is_parent' ,'$parent_id')";             
                else:
                $insert_user_query = "INSERT INTO users (username ,fullname , email , phone ,role , address ,  password , is_parent) VALUES ('$username','$fullname' , '$email' , '$phone' , '$role' , '$address' , '$password','$is_parent')";             
                endif;
                $result_of_the_query = $this->conn->query($insert_user_query);             
                if($result_of_the_query == false){
                    echo $this->conn->error;
                    die();
                }
                if($_POST['by_admin']){
                    return;
                }
                $_SESSION['username'] = $username;
                $_SESSION['success'] = 'You are logged in';
                $_SESSION['fullname'] = $fullname;
                $_SESSION['email'] = $email;
                $_SESSION['phone'] = $phone;
                $_SESSION['address'] = $address;
                $_SESSION['id'] = $this->conn->insert_id;   
                $_SESSION['role'] = $role;   

                header('location: index.php');
            }
    }

     function login(){
        $email =($_POST['email'])? $_POST['email'] : '';
        $password = ($_POST['password'])? $_POST['password']: '';

        if(empty($email)) {
            array_push($GLOBALS['errors'] , 'email is required'); 
        }
        if(empty($password)) {
            array_push($GLOBALS['errors'] , 'password is required'); 
        }

        if(empty($GLOBALS['errors'])){
          
            $password = md5($password);

         
            $sql = "SELECT * FROM users WHERE email='$email' AND password='$password';";
            $result = $this->conn->query($sql);
            if($result->num_rows > 0){
                $user = $result->fetch_assoc();

                if($user['role'] == 3 || $user['role'] == 1  || $user['role'] == 2){
                    $_SESSION['name'] =$user['username'];
                    $_SESSION['success'] = "now you are logged in";
                    $_SESSION['id'] = $user['userId'];
                    $_SESSION['fullname'] = $user['fullname'];
                    $_SESSION['email'] = $user['email'];     
                    $_SESSION['role'] = $user['role'];               
                    header('location: '.$GLOBALS['APP_URL'] .'/dashboard/index.php');
                    return;
                }
                $_SESSION['username'] = $user['username'];
                $_SESSION['fullname'] = $user['fullname'];
                $_SESSION['email'] = $user['email'];
                $_SESSION['phone'] = $user['phone'];
                $_SESSION['address'] = $user['address'];               
                 $_SESSION['success'] = "now you are logged in";
                $_SESSION['id'] = $user['userId'];
                $_SESSION['role'] = $user['role'];               

                header('location: index.php');
            }else{
                array_push($GLOBALS['errors'] , "wrong username or password");
            }
        }
     }
     function getUsers(){
         if($_SESSION['role'] == 1){
             $parent_id = $_SESSION['id'];
            $get_all_users = "SELECT * FROM users Where parent_id = $parent_id ";
         }else{
            $get_all_users = "SELECT * FROM users";
         }
        $users = $this->conn->query($get_all_users);
        return $users;
     }
     
     function getUserById($id){
        $userById = "SELECT * FROM users Where userId=$id LIMIT 1 ";
        $user = $this->conn->query($userById)->fetch_assoc();
        return $user;
     }

    function userUpdate(){
        $fullname =(!empty($_POST['fullname']))? $_POST['fullname'] : '';
        $email =(!empty($_POST['email']))? $_POST['email'] : '';
        $address =(!empty($_POST['address']))? $_POST['address'] : '';
        $user_id =(!empty($_POST['user_id']))? $_POST['user_id'] : '';
        $update_user_query = "UPDATE users SET  fullname = '$fullname' , email = '$email' , address = '$address' Where userId = $user_id";
        $user = $this->conn->query($update_user_query);
    }
    function deleteUser(){
        $user_id =(!empty($_POST['user_id']))? $_POST['user_id'] : '';
        $delete_user_query = "DELETE FROM users WHERE userId=$user_id";
        $user_delete = $this->conn->query($delete_user_query);
    }
    function unblockUser(){
        $user_id =(!empty($_POST['user_id']))? $_POST['user_id'] : '';
        $unblock_user_query = "UPDATE users SET   status = 0 Where userId = $user_id";
        $unblock = $this->conn->query($unblock_user_query);
    }
    function blockUser(){
        $user_id =(!empty($_POST['user_id']))? $_POST['user_id'] : '';
        $block_user_query = "UPDATE users SET   status = 1 Where userId = $user_id";
        $block = $this->conn->query($block_user_query);
    }
}

?>